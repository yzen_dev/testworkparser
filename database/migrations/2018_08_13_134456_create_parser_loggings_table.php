<?php

use App\ParserResult;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParserLoggingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parser_loggings', function (Blueprint $table) {
            $table->increments('id');
            $table->ipAddress('visitor');
            $table->unsignedInteger('parser_result_id');
            $table->foreign('parser_result_id')
                ->references('id')->on((new App\ParserResult)->getTable())
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parser_loggings');
    }
}
