@extends('layouts.app')

@section('content')

    <div class="container">
        <h3>Главная страница</h3>

        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Клиент</th>
                <th scope="col">Сайт</th>
                <th scope="col">Поисковые блоки</th>
                <th scope="col">Дата</th>
            </tr>
            </thead>
            <tbody>
            @foreach($loggingList as $key => $item)
                <tr>
                    <td scope="row">{{$item->id}}</td>
                    <td>{{$item->visitor}}</td>
                    <td>{{$item->searchResult->site}}</td>
                    <td>
                        @foreach(\json_decode($item->searchResult->search_blocks) as $block)
                            {{($block->type==='class')?'.':'#'}}{{$block->name}}
                        @endforeach
                    </td>
                    <td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at)->diffForHumans(\Carbon\Carbon::now())}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $loggingList->links() }}
    </div>
@endsection