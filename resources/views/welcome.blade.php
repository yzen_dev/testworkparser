@extends('layouts.app')

@section('content')
    <div class="container">
        <h3>Главная страница</h3>
        <p>
            Текст может быть любым и может включать одно или несколько url.<br> После url в тексте будет размещены одна
            или несколько вставок типа
            class=”class_name” или id=”id_name”
            что является идентификатором блока
        </p>

        <div class="form-group">
            <div class="form-group">
                <label for="textForParsing">Текст:</label>
            </div>
            <div class="form-group">
                <textarea class="form-control" rows="10" id="textForParsing">{{--
                --}}Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's https://www.sunrise-tour.ru/russia/chernoe-more/tury/ standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type id="main_content"  specimen book. It has survived class="main_block_of_content" not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was populari class="mboc_text" sed in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum                site
                    https://www.sunrise-tour.ru/online/company/vacancies/ id="main_content" class="descr"{{--
                --}}</textarea>
            </div>
            <div class="form-group">
                <button type="button" onclick="parseText()" class="btn btn-info">Разобрать</button>
            </div>
        </div>
        <h4>Результат</h4>
        <ul class="nav nav-tabs" id="resultNavbar">
        </ul>

        <div class="tab-content" id="resultTabs">
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" id="modalLoad">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Загрука</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Идет зарука данных, пожалуйста, подождите не много.</p>
                </div>
            </div>
        </div>
    </div>

    <script>
        function parseText() {
            $('#modalLoad').modal('show');
            let text = document.getElementById("textForParsing").value;
            axios.post("/parse", {
                text: text
            })
                .then(function (response) {
                    let resultNavbar = document.getElementById('resultNavbar');
                    let resultTabs = document.getElementById('resultTabs');
                    let links = response.data;
                    let i = 1;
                    resultNavbar.innerHTML = "";
                    resultTabs.innerHTML = "";
                    links.forEach(function(element) {
                        let navbar = document.createElement('li');
                        navbar.className = (i===1)?"active":"";
                        navbar.innerHTML = "<a data-toggle=\"tab\" href=\"#tab"+i+"\">"+i+" сайт</a>";
                        resultNavbar.appendChild(navbar);

                        let tab = document.createElement('div');
                        tab.className = "tab-pane fade"+((i===1)?" in active":"");
                        tab.setAttribute("id", "tab"+i);
                        tab.innerHTML = element.url+'<br>'+element.text;
                        resultTabs.appendChild(tab);
                        i++;
                    });
                    $('#modalLoad').modal('hide');
                })
                .catch(function (error) {
                    alert('Произошла ошибка. Пожалуйста, обновите страницу!');
                    $('#modalLoad').modal('hide');
                    console.log(error);
                });
        }
    </script>
@endsection