<?php
namespace App\Utilities\PHPHtmlParser\Exceptions;

/**
 * Class CircularException
 *
 * @package PHPHtmlParser\Exceptions
 */
final class CircularException extends \Exception
{
}
