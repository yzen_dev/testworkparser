<?php
namespace App\Utilities\PHPHtmlParser\Exceptions;

/**
 * Class ChildNotFoundException
 *
 * @package PHPHtmlParser\Exceptions
 */
final class ChildNotFoundException extends \Exception
{
}

