<?php
namespace App\Utilities\PHPHtmlParser\Exceptions;

/**
 * Class EmptyCollectionException
 *
 * @package PHPHtmlParser\Exceptions
 */
final class EmptyCollectionException extends \Exception
{
}
