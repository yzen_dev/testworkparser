<?php
namespace App\Utilities\PHPHtmlParser\Exceptions;

/**
 * Class StrictException
 *
 * @package PHPHtmlParser\Exceptions
 */
final class StrictException extends \Exception
{
}
