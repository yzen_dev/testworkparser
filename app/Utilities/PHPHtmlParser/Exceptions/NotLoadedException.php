<?php
namespace App\Utilities\PHPHtmlParser\Exceptions;

/**
 * Class NotLoadedException
 *
 * @package PHPHtmlParser\Exceptions
 */
final class NotLoadedException extends \Exception
{
}
