<?php
namespace App\Utilities\PHPHtmlParser\Exceptions;

/**
 * Class ParentNotFoundException
 *
 * @package PHPHtmlParser\Exceptions
 */
final class ParentNotFoundException extends \Exception
{
}
