<?php
namespace App\Utilities\PHPHtmlParser\Exceptions;

/**
 * Class CurlException
 *
 * @package PHPHtmlParser\Exceptions
 */
class CurlException extends \Exception
{
}
