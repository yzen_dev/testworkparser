<?php
namespace App\Utilities\PHPHtmlParser\Exceptions;

/**
 * Class UnknownChildTypeException
 *
 * @package PHPHtmlParser\Exceptions
 */
final class UnknownChildTypeException extends \Exception
{
}
