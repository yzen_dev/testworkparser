<?php

namespace App\Services;

use App\ParserLogging;
use App\ParserResult;
use App\Utilities\PHPHtmlParser\Dom;
use Illuminate\Support\Facades\Request;

class Parsing
{
    /**
     * Разбор текста
     * @param string Текст для парсера
     * @return array Полученный с сайта текст
     */
    public static function parsingText($text): array
    {
        $urlBlocks = self::getUrlList($text);
        $result = [];

        foreach ($urlBlocks as $url) {
            $dom = new Dom;
            $dom = $dom->loadFromUrl($url['url']);
            $text = self::parseTextByBlockName($url['blocks'], 0, $dom);
            $text = strip_tags($text, '<br><a><i><label><span><p><table><tr><td><thead><tbody><h1><h2><h3><h4><h5>');
            self::loggingParse($url,$text);
            $result[] = [
                'url' => $url['url'],
                'text' => $text
            ];
        }
        return $result;
    }

    /**
     * Логирование результата парсинга.
     * @param $url int Id удаляемого района.
     * @param $text string Id удаляемого района.
     */
    private static function loggingParse($url,$text)
    {
        $parserResult = new ParserResult;
        $parserResult->site = $url['url'];
        $parserResult->search_blocks = \json_encode($url['blocks']);
        $parserResult->result_text = $text;
        $parserResult->save();

        $parserLogging = new ParserLogging;
        $parserLogging->visitor = Request::ip();
        $parserLogging->parser_result_id = $parserResult->id;
        $parserLogging->save();
    }

    /**
     * Получение текста по иерархие блоков
     * @param $blocks array Массив искомых блоков.
     * @param $i int Id блока с которого начинается поиск.
     * @param $domBlock Dom DOM дерево страницы.
     * @return string Найденный текст
     */
    private static function parseTextByBlockName($blocks, $i, $domBlock)
    {
        $text = "";
        $type = ($blocks[$i]['type'] === 'id') ? '#' : ".";
        try {
            $blockParsing = $domBlock->find($type . $blocks[$i]['name']);
        } catch (\Throwable $e) {
            return 'Не удалось найти блок';
        }
        foreach ($blockParsing as $key => $class) {
            if ($i < \count($blocks) - 1) {
                $text .= self::parseTextByBlockName($blocks, $i + 1, $class);
            } else {
                $text .= $class->innerHtml;
            }
        }
        return $text;
    }

    /**
     * Получение списка ссылок с их блоками
     * @param string Текст для парсера
     * @return array|null Полученный массив ссылок
     */
    private
    static function getUrlList(
        $text
    ) {
        $pattern = "/https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b[-a-zA-Z0-9@:%_\+.~#?&\/=]*/m";
        $urlBlocks = preg_split($pattern, $text);//Получаем все текстовые блоки между ссылками
        unset($urlBlocks[0]);// Текст до 1 ссылки нам не интересен

        preg_match_all($pattern, $text, $urlList);// получение всех ссылок
        $urlList = $urlList[0];//Забираем только полное совпадение
        $result = [];
        foreach ($urlList as $key => $item) {
            $blocks = self::getBlocks($urlBlocks[$key + 1]); // потому что первый элемент мы удалили, и индексы остаются старые
            $result[] = [
                'url' => $item,
                'blocks' => $blocks
            ];
        }
        return $result;
    }

    /**
     * Получение всех указателей для поиска блока
     * @param string Текст для парсера
     * @return array|null Полученный массив блоков
     */
    private
    static function getBlocks(
        $text
    ) {
        $pattern = "/(class|id)=\"([a-zA-Z0-9_.-]*)\"/m";
        preg_match_all($pattern, $text, $blocks);
        $result = [];
        foreach ($blocks[2] as $key => $item) {
            $result[] = [
                'type' => $blocks[1][$key],
                'name' => $item
            ];
        }
        return $result;
    }
}