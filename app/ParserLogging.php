<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ParserLogging
 * @property string visitor
 * @property int parser_result_id
 * @property ParserResult searchResult
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class ParserLogging extends Model
{
    protected $fillable = ['visitor', 'parser_result_id'];

    public function searchResult()
    {
        return $this->hasOne('App\ParserResult','id','parser_result_id');
    }
}
