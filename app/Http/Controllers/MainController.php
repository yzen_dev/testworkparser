<?php

namespace App\Http\Controllers;


use App\ParserLogging;
use App\Services\Parsing;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class MainController extends Controller
{
    /**
     * Отоброжение главной страницы
     */
    public function indexView()
    {
        return view('welcome');
    }

    /**
     * Отоброжение страницы с историей запросов
     */
    public function loggingView()
    {
        Carbon::setLocale('ru');
        $loggingList = ParserLogging::with('searchResult')
            ->paginate(10);
        return view('logging',[
                'loggingList' => $loggingList
        ]);
    }

    /**
     * Парсер страницы
     * @param Request $response
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function parseSite(Request $response)
    {
        $result = Parsing::parsingText($response->get('text'));
        return response($result, 200);
    }
}
