<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ParserResult
 * @property string site
 * @property string search_blocks
 * @property string result_text
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 */
class ParserResult extends Model
{
    protected $fillable = ['site', 'search_blocks', 'result_text'];
}
